apiVersion: v1
kind: Template
metadata:
  name: "mariadb"
  labels:
    app: ${DEPLOYMENT_NAME}
    template: mariadb-template
  annotations:
    openshift.io/display-name: Maria DB
    iconClass: proteon-icon-liferay-firelay
    template.openshift.io/bindable: "true"
    description: >-
      Maria DB.
    openshift.io/long-description: >-
      Environments created using this template will generate an instance of Maria DB. This template is well suited for Acceptance and Production environments.
    tags: "mariadb, proteon, firelay"
    openshift.io/provider-display-name: "Proteon, Inc."
    openshift.io/documentation-url: "https://git.proteon.nl/hypercube/openshift-templates/liferay/README.md"
    openshift.io/support-url: "https://www.proteon.com/contact/"
objects:
  ##### MariaDB Deployment #####
  - apiVersion: extensions/v1beta1
    kind: Deployment
    metadata:
      name: ${DEPLOYMENT_NAME}-mariadb
    spec:
      template:
        metadata:
          labels:
            tier: mariadb
            app: ${DEPLOYMENT_NAME}
            env: acc
        spec:
          containers:
            - name: mariadb
              image: ${MARIADB_IMAGE}
              resources:
                limits:
                    memory: "${MARIADB_MEMORY_LIMIT}Mi"
                    cpu: "${MARIADB_CPU_LIMIT}"
                requests: 
                    memory: ${MARIADB_MEMORY_REQUEST}Mi
                    cpu: "${MARIADB_CPU_REQUEST}"
              readinessProbe:
                exec:
                  command:
                    - /bin/sh
                    - '-i'
                    - '-c'
                    - >-
                      MYSQL_PWD="$MYSQL_PASSWORD" mysql -h 127.0.0.1 -u $MYSQL_USER
                      -D $MYSQL_DATABASE -e 'SELECT 1'
                failureThreshold: 3
                initialDelaySeconds: 5
                periodSeconds: 10
                successThreshold: 1
                timeoutSeconds: 1
              livenessProbe:
                failureThreshold: 3
                initialDelaySeconds: 30
                periodSeconds: 10
                successThreshold: 1
                tcpSocket:
                  port: 3306
                timeoutSeconds: 1
              ports:
                - containerPort: 3306
                  protocol: TCP
              env:
                - name: MYSQL_ROOT_PASSWORD
                  valueFrom:
                    secretKeyRef:
                      name: ${DEPLOYMENT_NAME}-mariadb
                      key: ROOT_PASSWORD
                - name: MYSQL_USER
                  valueFrom:
                    secretKeyRef:
                      name: ${DEPLOYMENT_NAME}-mariadb
                      key: DB_USER
                - name: MYSQL_PASSWORD
                  valueFrom:
                    secretKeyRef:
                      name: ${DEPLOYMENT_NAME}-mariadb
                      key: DB_PASSWORD
                - name: MYSQL_DATABASE
                  valueFrom:
                    secretKeyRef:
                      name: ${DEPLOYMENT_NAME}-mariadb
                      key: DB_NAME
              volumeMounts:
                # - name: configmap
                #   mountPath: /etc/mysql/my.cnf
                #   subPath: my.cnf
                - mountPath: /etc/my.cnf.d/60-openshift.cnf
                  name: configmap
                  subPath: 60-openshift.cnf
                - name: mariadb-data
                  mountPath: /var/lib/mysql/data
          volumes:
            - name: configmap
              configMap:
                defaultMode: 420
                name: ${DEPLOYMENT_NAME}-mariadb
            - name: mariadb-data
              persistentVolumeClaim:
                claimName: ${DEPLOYMENT_NAME}-mariadb

    ### MariaDB ConfigMap ###
  - apiVersion: v1
    kind: ConfigMap
    metadata:
      name: ${DEPLOYMENT_NAME}-mariadb
      labels:
        app: ${DEPLOYMENT_NAME}-mariadb
        tier: mariadb
    data:
      60-openshift.cnf: |+
        [client]
        default-character-set = utf8
        [mysql]
        default-character-set = utf8
        [mysqld]
        character-set-server=utf8
        collation-server=utf8_general_ci
        ignore_db_dirs=lost+found,.trashcan
        init-connect='SET NAMES utf8'


  ### MariaDB PVC ###
  - apiVersion: v1
    kind: PersistentVolumeClaim
    metadata:
      name: ${DEPLOYMENT_NAME}-mariadb
      labels:
        tier: mariadb
        app: ${DEPLOYMENT_NAME}
    spec:
      accessModes:
        - ReadWriteOnce
      resources:
        requests:
          storage: ${MARIADB_STORAGE_SIZE}Gi

    ### MariaDB Secret ###
  - apiVersion: v1
    kind: Secret
    metadata:
      name: ${DEPLOYMENT_NAME}-mariadb
      labels:
        tier: mariadb
        app: ${DEPLOYMENT_NAME}
    stringData:
      ROOT_PASSWORD : ZmlyZWxheXN1cGVyc2VjdXJl
      DB_USER: ${DB_USER}
      DB_PASSWORD: ${DB_PASSWORD}
      DB_NAME: ${DB_NAME}

    ### MariaDB Service ###
  - apiVersion: v1
    kind: Service
    metadata:
      name: ${DEPLOYMENT_NAME}-mariadb
      labels:
        tier: mariadb
        app: ${DEPLOYMENT_NAME}
    spec:
      ports:
        - port: 3306
      selector:
        tier: mariadb
        app: ${DEPLOYMENT_NAME}

parameters:
  - name: DEPLOYMENT_NAME
    description: Name of deployment
    required: true
    value: deltares
  - name: DB_NAME
    description: Database name for liferay
    required: true
    value: liferay
  - name: DB_PASSWORD
    description: Database password for liferay if not provided a random one will be generated
    required: true
    value: liferay
  - name: DB_USER
    description: Database user for liferay
    required: true
    value: liferay
  - name: MARIADB_STORAGE_SIZE
    description: Storage size to be used in Gi
    required: true
    value: "1"
  - name: MARIADB_IMAGE
    description: Image used for mariadb database
    required: true
    value: registry.access.redhat.com/rhscl/mariadb-102-rhel7
  - name: MARIADB_MEMORY_REQUEST
    description: Requested memory for mariadb instance *Remeber to add Mi or Gi
    value: "128"
  - name: MARIADB_MEMORY_LIMIT
    description: Max Memory limit for mariadb instance *Remeber to add Mi or Gi
    value: "512"
  - name: MARIADB_CPU_REQUEST
    description: Requested CPU for mariadb instance in Core units
    value: "0.5"
  - name: MARIADB_CPU_LIMIT
    description: Max CPU limit for mariadb instance in Core units
    value: "1"
