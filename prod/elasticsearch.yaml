apiVersion: v1
kind: Template
metadata:
  name: "elasticsearch-cluster"
  labels:
    app: elasticsearch-cluster
    template: elasticsearch-cluster
  annotations:
    openshift.io/display-name: Elasticsearch 6 Cluster
    iconClass: proteon-icon-elasticsearch-firelay
    template.openshift.io/bindable: "true"
    description: >-
      Elasticsearch Cluster
    openshift.io/long-description: >-
      Template to deploy an elasticsearch cluster ready for production purposes
    tags: "elasticsearch, proteon, firelay, java"
    openshift.io/provider-display-name: "Proteon, Inc."
    openshift.io/documentation-url: "https://git.firelay.com/openshift/templates/README.md"
    openshift.io/support-url: "https://www.proteon.com/contact/"
objects:
### ElasticSearch ###
  - apiVersion: apps/v1
    kind: StatefulSet
    metadata:
      name: ${DEPLOYMENT_NAME}-es
      labels:
        env: prod
    spec:
      selector:
        matchLabels:
          tier: elasticsearch
          version: ${ELASTICSEARCH_IMAGE_TAG}
          app: ${DEPLOYMENT_NAME}
      serviceName: "${DEPLOYMENT_NAME}-es"
      replicas: "${{REPLICAS}}"
      template:
        metadata:
          labels:
            tier: elasticsearch
            version: ${ELASTICSEARCH_IMAGE_TAG}
            app: ${DEPLOYMENT_NAME}
            env: prod
        spec:
          serviceAccount: ${SERVICE_ACCOUNT}
          serviceAccountName: ${SERVICE_ACCOUNT}
          terminationGracePeriodSeconds: 20
          containers:
            - image: ${ELASTICSEARCH_IMAGE}:${ELASTICSEARCH_IMAGE_TAG}
              imagePullPolicy: Always
              name: elasticsearch
              resources:
                limits:
                  cpu: "${ELASTICSEARCH_CPU}"
                  memory: ${ELASTICSEARCH_MEMORY}
                requests:
                  cpu: "${ELASTICSEARCH_CPU}"
                  memory: ${ELASTICSEARCH_MEMORY}
              env:
                - name: NAMESPACE
                  valueFrom:
                    fieldRef:
                      fieldPath: metadata.namespace
                - name: NODE_NAME
                  valueFrom:
                    fieldRef:
                      fieldPath: metadata.name
                - name: CLUSTER_NAME
                  value: ${ES_CLUSTER_NAME}
                - name: NODE_MASTER
                  value: "true"
                - name: NODE_INGEST
                  value: "false"
                - name: HTTP_ENABLE
                  value: "true"
                - name: ES_JAVA_OPTS
                  value: ${ES_JAVA_OPTS}
                - name: NUMBER_OF_MASTERS
                  value: ${NUMBER_OF_MASTERS}
                - name: DISCOVERY_SERVICE
                  value: ${DEPLOYMENT_NAME}-es
                - name: ES_PLUGINS_INSTALL
                  value: "analysis-icu,analysis-kuromoji,analysis-smartcn,analysis-stempel"
                - name: PROCESSORS
                  valueFrom:
                    resourceFieldRef:
                      resource: limits.cpu
              ports:
                - containerPort: 9200
                  name: http
                  protocol: TCP
              terminationMessagePath: /dev/termination-log
              # readinessProbe:
              #   # so we can check every 10 sec for 15+ min
              #   failureThreshold: 20
              #   httpGet:
              #     path: /
              #     port: 9200
              #     scheme: HTTP
              #   initialDelaySeconds: 30
              #   periodSeconds: 10
              #   successThreshold: 1
              #   timeoutSeconds: 15
              # livenessProbe:
              #   failureThreshold: 3
              #   httpGet:
              #     path: /
              #     port: 9200
              #     scheme: HTTP
              #   initialDelaySeconds: 180
              #   periodSeconds: 60
              #   timeoutSeconds: 15
              volumeMounts:
                - mountPath: /data
                  name: ${DEPLOYMENT_NAME}-es
      volumeClaimTemplates:
        - metadata:
            name: ${DEPLOYMENT_NAME}-es
            labels:
              tier: elasticsearch
              version: ${ELASTICSEARCH_IMAGE_TAG}
              app: ${DEPLOYMENT_NAME}
              env: prod
          spec:
            accessModes:
              - ReadWriteOnce
            storageClassName: ${ELASTICSEARCH_STORAGE_CLASS}
            resources:
              requests:
                storage: ${ELASTICSEARCH_STORAGE_SIZE}Gi
                

  ### ElasticSearch Service ###
  - apiVersion: v1
    kind: Service
    metadata:
      name: ${DEPLOYMENT_NAME}-es
      labels:
        tier: elasticsearch
        version: ${ELASTICSEARCH_IMAGE_TAG}
        app: ${DEPLOYMENT_NAME}
        env: prod
    spec:
      ports:
        - name: transport
          port: 9300
          protocol: TCP
          targetPort: 9300
        - name: web
          port: 9200
          protocol: TCP
          targetPort: 9200
      selector:
        tier: elasticsearch
        version: ${ELASTICSEARCH_IMAGE_TAG}
        app: ${DEPLOYMENT_NAME}


parameters:
  - name: DEPLOYMENT_NAME
    description: Name of deployment
    required: true
    value: deltares
  - name: IMAGE_PULL_POLICY
    description: Image Pull Policy for pod
    required: true
    value: IfNotPresent
  - name: ES_CLUSTER_NAME
    description: Elasticsearch cluster name
    required: true
    value: LiferayES
  - name: ELASTICSEARCH_IMAGE
    description: Image used for Elasticsearch
    required: true
    value: registry.firelay.com/container-engine-images/elasticsearch
  - name: ELASTICSEARCH_IMAGE_TAG
    description: Image used for Elasticsearch
    required: true
    value: "6.1.4"
  

  - name: SERVICE_ACCOUNT
    description: Service Account used to deploy liferay instance
    required: true
    value: firelay

  - name: REPLICAS
    description: Number of replicas for ElasticSearch nodes
    required: true
    value: "1"

  - name: NUMBER_OF_MASTERS
    descrioption: Minumum number of masters required to make cluster available, use "(REPLICAS / 2) + 1"
    required: true
    value: "1"

  - name: ES_JAVA_OPTS
    description: Java Opts for elasticsearch, make sure memory settins are 80% the full memory for the pod
    required: true
    value: "-Xms3800m -Xmx3800m"
  - name: ELASTICSEARCH_MEMORY
    description: Max Memory limit for elasticsearch instance *Remeber to add Mi or Gi
    required: true
    value: "6Gi"
  - name: ELASTICSEARCH_CPU
    description: Max CPU limit for elasticsearch instance in Core units
    required: true
    value: "2"
  - name: ELASTICSEARCH_STORAGE_CLASS
    value: glusterfs-storage-block
  - name: ELASTICSEARCH_STORAGE_SIZE
    description: Storage size to be used in Gi
    required: true
    value: "1"
